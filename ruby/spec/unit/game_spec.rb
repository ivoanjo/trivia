require 'ugly_trivia/game'

RSpec.describe UglyTrivia::Game do
  describe '#how_many_players' do
    context 'when the game is initialized' do
      specify do
        expect(subject.how_many_players).to be 0
      end
    end

    it 'returns the current number of players' do
      (1..6).each do |n|
        subject.add("Player #{n}")
        expect(subject.how_many_players).to be n
      end
    end
  end

  describe '#is_playable?' do
    context 'when game has no players' do
      specify do
        expect(subject.is_playable?).to be false
      end
    end

    context 'when game has one player' do
      before do
        subject.add('Player 1')
      end

      specify do
        expect(subject.is_playable?).to be false
      end
    end

    context 'when game has 2..6 players' do
      it 'returns true' do
        subject.add("Player 1")

        (2..6).each do |n|
          subject.add("Player #{n}")
          expect(subject.is_playable?).to be true
        end
      end
    end
  end

  describe '#add' do
    specify do
      expect(subject.add("Player 1")).to be true
    end

    context 'when trying to add more than 6 players' do
      before do
        6.times.with_index do |n|
          subject.add("Player #{n}")
        end
      end

      specify do
        expect(subject.add("Player 7")).to be false
      end

      it 'does not add more than 6 players' do
        subject.add("Player 7")

        expect(subject.how_many_players).to be 6
      end
    end
  end

  describe 'enforcement of minimum of 2 players' do
    before do
      subject.add('Player 1')
    end

    specify do
      expect { subject.roll(1) }.to raise_error(UglyTrivia::Game::NotEnoughPlayersException)
    end

    specify do
      expect { subject.was_correctly_answered }.to raise_error(UglyTrivia::Game::NotEnoughPlayersException)
    end

    specify do
      expect { subject.wrong_answer }.to raise_error(UglyTrivia::Game::NotEnoughPlayersException)
    end
  end
end
